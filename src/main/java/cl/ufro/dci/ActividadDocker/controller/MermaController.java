package cl.ufro.dci.ActividadDocker.controller;

import cl.ufro.dci.ActividadDocker.model.Merma;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("mermas")
public class MermaController {

    private List<Merma> mermas = new ArrayList<>();

    public MermaController(){
        Merma merma1 = new Merma("Fideos","Paquete Roto",3);
        Merma merma2 = new Merma("Yogurt","Fecha Expirada",10);
        Merma merma3 = new Merma("Quix","Caja Perdida",10);

        mermas.add(merma1);
        mermas.add(merma2);
        mermas.add(merma3);
    }

    @GetMapping()
    public List<Merma> mermas(){
        return mermas;
    }


}
