package cl.ufro.dci.ActividadDocker.controller;


import cl.ufro.dci.ActividadDocker.model.UsuarioReponedor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("reponedores")
public class UsuarioReponedorController {

    private List<UsuarioReponedor> reponedores = new ArrayList<>();

    public UsuarioReponedorController(){
        UsuarioReponedor reponedor1 = new UsuarioReponedor("Martina Carro","20000000-0","MC20","1234","Abarrotes");
        UsuarioReponedor reponedor2 = new UsuarioReponedor("Virginia Bosch","19000000-0","VB19","5678","Fiambreria");
        UsuarioReponedor reponedor3 = new UsuarioReponedor("Arantxa Arranz","18000000-0","AA18","9101","Lacteos");

        reponedores.add(reponedor1);
        reponedores.add(reponedor2);
        reponedores.add(reponedor3);
    }

    @GetMapping()
    public List<UsuarioReponedor> Reponedores(){
        return reponedores;
    }


}
