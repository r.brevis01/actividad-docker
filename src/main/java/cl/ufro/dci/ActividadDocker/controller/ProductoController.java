package cl.ufro.dci.ActividadDocker.controller;

import cl.ufro.dci.ActividadDocker.model.Producto;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("productos")
public class ProductoController {

    private List<Producto> productos = new ArrayList<>();

    public ProductoController(){
        Producto producto1 = new Producto("Fideos","Abarrotes",4,790,1);
        Producto producto2 = new Producto("Yogurt","Lacteos",0,210,10);
        Producto producto3 = new Producto("Arroz","Abarrotes",10,500,2);
        Producto producto4 = new Producto("Leche En Polvo","Abarrotes",2,5990,5);
        Producto producto5 = new Producto("Confort","Abarrotes",50,800,4);
        Producto producto6 = new Producto("Quix","Productos de Limpieza",12,1000,9);
        Producto producto7 = new Producto("Queso","Fiambreria",100,1990,12);
        Producto producto8 = new Producto("Pan Marraqueta","11",999,990,11);

        productos.add(producto1);
        productos.add(producto2);
        productos.add(producto3);
        productos.add(producto4);
        productos.add(producto5);
        productos.add(producto6);
        productos.add(producto7);
        productos.add(producto8);
    }

    @GetMapping("")
    public List<Producto> productos(){
        return productos;
    }


}
