package cl.ufro.dci.ActividadDocker.controller;


import cl.ufro.dci.ActividadDocker.model.UsuarioCajero;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("cajeros")
public class UsuarioCajeroController {

    private List<UsuarioCajero> cajeros = new ArrayList<>();

    public UsuarioCajeroController(){
        UsuarioCajero cajero1 = new UsuarioCajero("Bartolome Castaño","17000000-0","BC17","2345",10);
        UsuarioCajero cajero2 = new UsuarioCajero("Victorina Valdivia","16000000-0","VV16","6789",0);
        UsuarioCajero cajero3 = new UsuarioCajero("Federico Galera","15000000-0","FG15","1012",999);
        UsuarioCajero cajero4 = new UsuarioCajero("Letícia Alamo","14000000-0","LA14","3456",100);
        UsuarioCajero cajero5 = new UsuarioCajero("Vanessa Saura","13000000-0","VS13","7891",1);

        cajeros.add(cajero1);
        cajeros.add(cajero2);
        cajeros.add(cajero3);
        cajeros.add(cajero4);
        cajeros.add(cajero5);

    }

    @GetMapping()
    public List<UsuarioCajero> cajeros(){
        return cajeros;
    }



}
