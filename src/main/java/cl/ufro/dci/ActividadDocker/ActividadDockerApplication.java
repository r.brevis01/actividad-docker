package cl.ufro.dci.ActividadDocker;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
public class ActividadDockerApplication {

	public static void main(String[] args) {
		SpringApplication.run(ActividadDockerApplication.class, args);
	}

}
