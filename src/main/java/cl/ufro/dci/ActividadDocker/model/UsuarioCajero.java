package cl.ufro.dci.ActividadDocker.model;

public class UsuarioCajero {

    private String nombre;
    private String rut;
    private String usuario;
    private String contrasenna;
    private int ventas;

    public UsuarioCajero(String nombre, String rut, String usuario, String contrasenna, int ventas) {
        this.nombre = nombre;
        this.rut = rut;
        this.usuario = usuario;
        this.contrasenna = contrasenna;
        this.ventas = ventas;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getRut() {
        return rut;
    }

    public void setRut(String rut) {
        this.rut = rut;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getContrasenna() {
        return contrasenna;
    }

    public void setContrasenna(String contrasenna) {
        this.contrasenna = contrasenna;
    }

    public int getVentas() {
        return ventas;
    }

    public void setVentas(int ventas) {
        this.ventas = ventas;
    }
}
