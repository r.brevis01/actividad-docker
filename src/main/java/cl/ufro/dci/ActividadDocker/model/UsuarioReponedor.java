package cl.ufro.dci.ActividadDocker.model;

public class UsuarioReponedor {

    private String nombre;
    private String rut;
    private String usuario;
    private String contrasenna;
    private String seccion;

    public UsuarioReponedor(String nombre, String rut, String usuario, String contrasenna, String seccion) {
        this.nombre = nombre;
        this.rut = rut;
        this.usuario = usuario;
        this.contrasenna = contrasenna;
        this.seccion = seccion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getRut() {
        return rut;
    }

    public void setRut(String rut) {
        this.rut = rut;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getContrasenna() {
        return contrasenna;
    }

    public void setContrasenna(String contrasenna) {
        this.contrasenna = contrasenna;
    }

    public String getSeccion() {
        return seccion;
    }

    public void setSeccion(String seccion) {
        this.seccion = seccion;
    }
}
