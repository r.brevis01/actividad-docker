FROM openjdk:17.0-oracle
COPY "./target/ActividadDocker.jar" "app.jar"
EXPOSE 8080
ENTRYPOINT ["java","-jar","app.jar"]